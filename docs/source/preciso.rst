Python API
==========

Below is the automatically generated documentation of PreciSo.


preciso
---------------------

.. automodule:: preciso.preciso
    :members:
    :undoc-members:
    :show-inheritance:

preciso.results
---------------------

.. automodule:: preciso.results
    :members:
    :undoc-members:
    :show-inheritance:

preciso.run
---------------------

.. automodule:: preciso.run
    :members:
    :undoc-members:
    :show-inheritance:

preciso.HDF5
---------------------

.. automodule:: preciso.HDF5
    :members:
    :undoc-members:
    :show-inheritance:
