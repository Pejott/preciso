Updates
=======

Version 1.0.9
-------------

- Bug correction in timestep management for 1 node version
- new executables for macOS, Win and linux



Version 0.1.6
-------------

- Support for noPrecipitation keyword
- Initial distrib was fixed to support empty distributions & classes.
- Added SiteSaturation yes-no keyword for saturation of nucleation sites in heterogeneous precipitation.


Version 0.1.5.1
---------------

- Fixed logo issue

Version 0.1.4
-------------

- **Added MANIFEST.in** to allow uploading images, docs, ... via PyPi

Version 0.1.3
-------------

- **New executable for mac OS**

- **preciso.version()** added.

- **preciso.add_logo** refactored and docstring cleaned up.

